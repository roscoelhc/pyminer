'''
有一个问题：dialog的加载是会消耗时间的。建议对于dialog，传递类而非对象，用到的时候再动态加载。
'''

from PyQt5.QtWidgets import QDialog, QHBoxLayout, QPushButton
from pyminer2.ui.generalwidgets import PMToolButton
from pyminer2.extensions.extensionlib.pmext import PluginInterface


class Extension:
    def on_load(self):
        print("被加载!")

    def on_install(self):
        print('被安装')

    def on_uninstall(self):
        print("被卸载")


class Interface:
    def hello(self):
        print("Hello")


class DemoToolButton(PMToolButton):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setText('插件向\n工具栏插入\n控件')
        self.clicked.connect(self.show_dialog)
    def show_dialog(self):
        PluginInterface.get_main_window().dialogs['new_dialog'].show()


class DemoToolDialog(QDialog):
    def __init__(self):
        super().__init__()
        layout = QHBoxLayout()
        layout.addWidget(QPushButton('aaa'))
        self.setLayout(layout)

if __name__=='__main__':
    from PyQt5.QtWidgets import QApplication
    import sys
    a=QApplication(sys.argv)
    w = DemoToolDialog()
    w.show()
    sys.exit(a.exec_())