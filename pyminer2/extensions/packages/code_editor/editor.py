'''
作者：侯展意
1295752786@qq.com

代码高亮部分来源：
https://blog.csdn.net/xiaoyangyang20/article/details/68923133

代码交互逻辑、自动补全借用了Jedi库，能够达到不错的体验。
文本编辑器采用了一个QThread作为后台线程。
'''
import os
import re
import jedi
import time

from PyQt5.QtCore import QRegExp, Qt, QModelIndex, pyqtSignal, QThread
from PyQt5.QtWidgets import QApplication, QFileDialog, QTextEdit, QTabWidget, \
    QMessageBox, QListWidget, QListWidgetItem, QWidget, QHBoxLayout, QVBoxLayout, QLabel
from PyQt5.QtGui import QTextCursor, QKeyEvent, QMouseEvent
from typing import List, Tuple
from pyminer2.extensions.extensionlib.pmext import PluginInterface
from pyminer2.ui.generalwidgets import PythonHighlighter
from .syntaxana import getIndent

__version__ = "0.1"


class AutoCompThread(QThread):
    '''
    当一段时间没有补全需求之后，后台自动补全线程会进入休眠模式。下一次补全请求则会唤醒该后台线程。
    '''
    trigger = pyqtSignal(tuple, list)

    def __init__(self):
        super(AutoCompThread, self).__init__()
        self.text = ''
        self.text_cursor_pos = (0, 1)
        self.activated = True

    def run(self):
        text = ''
        last_complete_time = time.time()
        while (1):
            pos = self.text_cursor_pos
            if self.text == text:
                if time.time() - last_complete_time >= 30:
                    self.activated = False
                time.sleep(0.02 if self.activated else 0.5)
                continue

            try:
                script = jedi.Script(self.text)
                l = script.complete(*self.text_cursor_pos)
            except:
                import traceback
                print(self.text_cursor_pos)
                print(self.text)
                traceback.print_exc()
                l = []
            self.trigger.emit(pos, l)
            last_complete_time = time.time()

            self.activated = True
            text = self.text


class AutoCompList(QListWidget):
    def __init__(self, parent: 'PMCodeEdit' = None):
        super().__init__(parent)
        self._parent = parent
        self.autocomp_list: List[str] = []
        self.last_show_time = 0

    def can_show(self):
        if time.time() - self.last_show_time < 1:
            return False
        return True

    def show(self) -> None:
        self.last_show_time = time.time()
        super().show()

    def hide_autocomp(self):
        self.autocomp_list = []
        self.hide()

    def keyPressEvent(self, e: QKeyEvent) -> None:
        if self.isVisible():
            if e.key() == Qt.Key_Return or e.key() == Qt.Key_Tab:
                self._parent._insert_autocomp()
                self._parent.setFocus()
                e.accept()
                return
            elif e.key() == Qt.Key_Escape:
                self.hide()
                self._parent.setFocus()
                return
            elif e.key() == Qt.Key_Up or e.key() == Qt.Key_Down:
                super().keyPressEvent(e)
                e.accept()
                return
            elif e.key() == Qt.Key_Left or e.key() == Qt.Key_Right:
                self.hide_autocomp()
        super().keyPressEvent(e)
        e.ignore()


class PMCodeEditor(QWidget):
    def __init__(self, parent: 'PMCodeEditor' = None):
        super().__init__(parent=parent)
        self.edit = PMCodeEdit(self)
        self.line_number_area = QWidget()
        self.line_number_area.setMaximumHeight(60)
        self.line_number_area.setMinimumHeight(20)
        self.status_label = QLabel()
        line_number_area_layout = QHBoxLayout()
        line_number_area_layout.addWidget(self.status_label)
        line_number_area_layout.setContentsMargins(0, 0, 0, 0)
        self.line_number_area.setLayout(line_number_area_layout)
        self.modified_status_label = QLabel()
        self.modified_status_label.setText('未保存')
        line_number_area_layout.addWidget(self.modified_status_label)

        self.edit.cursorPositionChanged.connect(self.show_cursor_pos)
        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.edit)
        layout.addWidget(self.line_number_area)

        self.setLayout(layout)

    def show_cursor_pos(self):
        row = self.edit.textCursor().block().blockNumber()
        col = self.edit.textCursor().columnNumber()
        self.status_label.setText('行：{row},列:{col}'.format(row=row + 1, col=col + 1))


class PMCodeEdit(QTextEdit):
    signal_save = pyqtSignal(str)

    def __init__(self, parent=None):
        super(PMCodeEdit, self).__init__(parent)
        self.setLineWrapMode(QTextEdit.NoWrap)
        self.doc_tab_widget: 'PMCodeEditor' = parent
        self.filename = '*'
        self.path = ''
        self.modified = True
        self.highlighter = PythonHighlighter(self.document())
        self.setTabChangesFocus(False)

        self.textChanged.connect(self.on_text_changed)

        self.popup_hint_widget = AutoCompList(self)
        self.popup_hint_widget.doubleClicked.connect(self._insert_autocomp)
        self.popup_hint_widget.hide()

        self.autocomp_thread = AutoCompThread()
        self.autocomp_thread.trigger.connect(self.on_autocomp_signal_received)
        self.autocomp_thread.start()

    def on_autocomp_signal_received(self, text_cursor_pos: tuple, completions: List['jedi.api.classes.Completion']):
        '''
        当收到自动补全提示信号时，执行的函数。
        :param text_cursor_pos:
        :param completions:
        :return:
        '''
        current_cursor_pos = self._get_textcursor_pos()
        if current_cursor_pos[0] + 1 == text_cursor_pos[0] and current_cursor_pos[1] == text_cursor_pos[1]:
            if len(completions) == 1:
                if completions[0].name == self._get_hint():
                    self.hide_autocomp()
                    return

            self.autocomp_show(completions)
        else:
            self.hide_autocomp()

    def hide_autocomp(self):
        self.popup_hint_widget.hide_autocomp()

    def on_text_changed(self):
        self._get_textcursor_pos()
        cursor_pos = self.cursorRect()
        self.popup_hint_widget.setGeometry(cursor_pos.x(), cursor_pos.y(), 150, 200)
        self._request_autocomp()
        if self.modified == True:
            return
        else:
            self.modified = True
            self.updateUi()

    def _insert_autocomp(self, e: QModelIndex = None):
        row = self.popup_hint_widget.currentRow()
        if 0 <= row < len(self.popup_hint_widget.autocomp_list):
            self.insertPlainText(self.popup_hint_widget.autocomp_list[row])
            self.popup_hint_widget.hide()

    def _get_nearby_text(self):
        block_text = self.textCursor().block().text()
        col = self.textCursor().columnNumber()
        return block_text[:col]

    def _get_hint(self):
        block_text = self.textCursor().block().text()
        if block_text.lstrip().startswith('#'):  # 在注释中
            return ''
        col = self.textCursor().columnNumber()
        nearby_text = block_text[:col]
        hint = re.split('[.:;,?!\s \+ \- = \* \\ \/  \( \)\[\]\{\} ]', nearby_text)[-1]
        return hint

    def _request_autocomp(self):
        pos = self._get_textcursor_pos()
        nearby_text = self._get_nearby_text()
        hint = self._get_hint()

        if hint == '' and not nearby_text.endswith(('.', '\\\\', '/')):
            self.popup_hint_widget.hide_autocomp()
            return
        self.autocomp_thread.text_cursor_pos = (pos[0] + 1, pos[1])
        self.autocomp_thread.text = self.toPlainText()

    def autocomp_show(self, completions: list):
        self.popup_hint_widget.clear()
        l = []
        if len(completions) != 0:
            for completion in completions:
                l.append(completion.complete)
                self.popup_hint_widget.addItem(QListWidgetItem(completion.name))
            self.popup_hint_widget.show()
            self.popup_hint_widget.setFocus()
            self.popup_hint_widget.setCurrentRow(0)
        else:
            self.popup_hint_widget.hide()
        self.popup_hint_widget.autocomp_list = l

    def _get_textcursor_pos(self) -> Tuple[int, int]:
        return self.textCursor().blockNumber(), self.textCursor().columnNumber()

    def updateUi(self):
        if self.modified:
            text = '未保存'
        else:
            text = '已保存'
        self.doc_tab_widget.modified_status_label.setText(text)

    def mousePressEvent(self, a0: QMouseEvent) -> None:
        PluginInterface.show_tool_bar('code_editor_toolbar')
        if self.popup_hint_widget.isVisible():
            self.popup_hint_widget.hide_autocomp()
        super().mousePressEvent(a0)

    def keyPressEvent(self, event: QKeyEvent) -> None:
        k = event.key()
        if k == Qt.Key_Tab:
            self.on_tab()
            return
        elif k == Qt.Key_Backtab:
            self.on_back_tab()
            return
        elif k == Qt.Key_S and event.modifiers() == Qt.ControlModifier:
            self.save()
            return
        elif k == Qt.Key_Slash and event.modifiers() == Qt.ControlModifier:
            self.comment()
        elif k == Qt.Key_Return:
            self.on_return_pressed()
            event.accept()
            return
        elif k == Qt.Key_Backspace:
            self.on_backspace(event)
            event.accept()
            return

        super().keyPressEvent(event)

    def on_backspace(self, key_backspace_event: QKeyEvent):
        cursor = self.textCursor()
        cursor.beginEditBlock()
        text = cursor.block().text()
        nearby_text = self._get_nearby_text()
        print('nearby_text')
        if nearby_text.isspace():
            for i in range(4):
                cursor.deletePreviousChar()

        else:
            super().keyPressEvent(key_backspace_event)
        cursor.endEditBlock()

    def on_return_pressed(self):
        '''
        按回车换行的方法
        :return:
        '''
        cursor = self.textCursor()
        cursor.beginEditBlock()
        text = cursor.block().text()
        text, indent = getIndent(text)
        if text.endswith(':'):

            cursor.insertText('\n' + ' ' * (indent + 4))
        else:

            cursor.insertText('\n' + ' ' * indent)
        cursor.endEditBlock()

    def comment(self):
        cursor = self.textCursor()
        cursor.beginEditBlock()
        if cursor.hasSelection():
            start = cursor.anchor()
            end = cursor.position()

            if start > end:
                start, end = end, start

            cursor.clearSelection()

            cursor.setPosition(start)
            cursor.movePosition(QTextCursor.StartOfLine)
            start_line = cursor.blockNumber()

            start = cursor.position()  # 将光标移动到行首，获取行首的位置
            cursor.setPosition(end)  # 将光标设置到末尾
            cursor.movePosition(QTextCursor.StartOfLine)  # 将光标设置到选区最后一行
            end_line = cursor.blockNumber()  # 获取光标的行号

            cursor.setPosition(start)
            current_line = cursor.blockNumber()
            last_line = current_line
            print(start_line, end_line, current_line)
            while current_line <= end_line:
                line_text, indent = getIndent(cursor.block().text())
                if line_text.startswith('#'):
                    cursor.movePosition(QTextCursor.NextCharacter, QTextCursor.MoveAnchor, indent)
                    cursor.deleteChar()
                else:
                    cursor.insertText('#')
                cursor.movePosition(QTextCursor.StartOfLine)
                cursor.movePosition(QTextCursor.Down)
                current_line = cursor.blockNumber()
                last_line = current_line
            cursor.movePosition(QTextCursor.StartOfLine)
        else:
            cursor.movePosition(QTextCursor.StartOfLine)
            line_text, indent = getIndent(cursor.block().text())
            if line_text.startswith('#'):
                cursor.movePosition(QTextCursor.NextCharacter, QTextCursor.MoveAnchor, indent)
                cursor.deleteChar()
            else:
                cursor.insertText('#')
            pass

        cursor.endEditBlock()

    def on_back_tab(self):
        cursor = self.textCursor()
        if cursor.hasSelection():
            self.editUnindent()

        else:
            cursor = self.textCursor()
            cursor.clearSelection()

            cursor.movePosition(QTextCursor.StartOfBlock)

            for i in range(4):
                cursor.movePosition(QTextCursor.NextCharacter,
                                    QTextCursor.KeepAnchor, 1)
                if not cursor.selectedText().endswith(' '):
                    cursor.movePosition(QTextCursor.PreviousCharacter,
                                        QTextCursor.KeepAnchor, 1)
                    break
            # print('cursor.selected',cursor.selectedText())
            cursor.removeSelectedText()

    def on_tab(self):
        cursor = self.textCursor()
        if cursor.hasSelection():
            self.editIndent()
            return
        else:
            nearby_text = self._get_nearby_text()
            hint = self._get_hint()

            if hint == '' and not nearby_text.endswith(('.', '\\\\', '/')):
                cursor = self.textCursor()
                cursor.insertText("    ")
            else:
                self._request_autocomp()

    def editIndent(self):
        cursor = self.textCursor()
        cursor.beginEditBlock()
        if cursor.hasSelection():
            start = pos = cursor.anchor()
            start_line = self.document().findBlock(start)
            end = cursor.position()

            if start > end:
                start, end = end, start
                pos = start
            cursor.clearSelection()

            cursor.setPosition(end)
            cursor.movePosition(QTextCursor.StartOfLine)
            end = cursor.position()
            # print(end)
            cursor.setPosition(start)
            cursor.movePosition(QTextCursor.StartOfLine)
            start = cursor.position()

            cursor.setPosition(end)
            while pos >= start:
                # print(pos, start)
                cursor.insertText("    ")

                cursor.movePosition(QTextCursor.Up)
                cursor.movePosition(QTextCursor.StartOfLine)
                lastPos = pos
                pos = cursor.position()
                if lastPos == pos:
                    break

                print('end loop', pos, start)
            cursor.setPosition(start)
            cursor.movePosition(QTextCursor.NextCharacter,
                                QTextCursor.KeepAnchor, end - start)
        cursor.endEditBlock()
        return True

    def editUnindent(self):
        cursor = self.textCursor()
        cursor.beginEditBlock()
        if cursor.hasSelection():
            start = pos = cursor.anchor()
            end = cursor.position()
            if start > end:
                start, end = end, start
                pos = start
            cursor.clearSelection()
            cursor.setPosition(start)
            cursor.movePosition(QTextCursor.StartOfLine)
            start = cursor.position()
            cursor.setPosition(end)
            cursor.movePosition(QTextCursor.StartOfLine)
            end = cursor.position()
            print(start, end)
            while pos >= start:
                # print('a',start,pos)
                cursor.movePosition(QTextCursor.NextCharacter,
                                    QTextCursor.KeepAnchor, 4)
                if cursor.selectedText() == "    ":
                    cursor.removeSelectedText()
                cursor.movePosition(QTextCursor.Up)
                cursor.movePosition(QTextCursor.StartOfLine)
                lastpos = pos
                pos = cursor.position()
                if pos == lastpos:
                    break
            cursor.setPosition(start)
            cursor.movePosition(QTextCursor.NextCharacter,
                                QTextCursor.KeepAnchor, end - start)

        cursor.endEditBlock()

    def save(self):
        print(self.toPlainText())
        if not os.path.isfile(self.path):
            print(os.getcwd())
            path = QFileDialog.getSaveFileName(self, "选择保存的文件", '/home/hzy/Desktop', filter='*.py')[0]
            print('path', path)
            if not path:  # 说明对话框被关闭，未选择文件,则直接返回。
                return
            if not path.endswith('.py'):
                path += '.py'
            self.path = path
            self.filename = os.path.basename(path)

        with open(self.path, 'w') as f:
            f.write(self.toPlainText())
        self.modified = False
        self.updateUi()
        self.signal_save.emit(self.filename)

    def on_close_request(self):
        if self.modified == True:
            answer = QMessageBox.question(self, '保存文件', '%s有未保存的更改，是否要保存？' % self.filename,
                                          QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
            if answer == QMessageBox.No:
                return
            else:
                self.save()


class PMCodeEditTabWidget(QTabWidget):
    def __init__(self):
        super().__init__()

    def init_toolbar(self):
        '''
        新建一个toolbar并且插入到主界面中。
        Returns:

        '''

    def get_current_text_edit(self) -> PMCodeEdit:
        return self.currentWidget().edit

    def run(self):
        w: PMCodeEdit = self.get_current_text_edit()
        text = w.document().toPlainText()
        PluginInterface.get_console().execute_command(text, False)

    def do_comment(self):
        w: PMCodeEdit = self.get_current_text_edit()
        w.comment()

    def do_indent(self):
        w: PMCodeEdit = self.get_current_text_edit()
        w.editIndent()

    def do_unindent(self):
        w: PMCodeEdit = self.get_current_text_edit()
        w.editUnindent()

    def setup_ui(self):
        self.init_toolbar()

        self.create_new_editor_tab()

        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self.on_tab_close_request)
        PluginInterface.get_toolbar_widget('code_editor_toolbar', 'button_new_script').clicked.connect(
            lambda: self.create_new_editor_tab())
        PluginInterface.get_toolbar_widget('code_editor_toolbar', 'button_open_script').clicked.connect(self.open_file)
        PluginInterface.get_toolbar_widget('code_editor_toolbar', 'button_run_script').clicked.connect(self.run)
        PluginInterface.get_toolbar_widget('code_editor_toolbar', 'button_indent').clicked.connect(self.do_indent)
        PluginInterface.get_toolbar_widget('code_editor_toolbar', 'button_unindent').clicked.connect(self.do_unindent)
        PluginInterface.get_toolbar_widget('code_editor_toolbar', 'button_comment').clicked.connect(self.do_comment)
        PluginInterface.get_toolbar_widget('code_editor_toolbar', 'button_uncomment').clicked.connect(self.do_comment)

    def create_new_editor_tab(self, text='', path='', filename='*', modified: bool = True):
        editor_widget = PMCodeEditor(self)
        edit = editor_widget.edit
        edit.path = path
        edit.filename = filename
        edit.signal_save.connect(self.refresh_modified_status)
        edit.setText(text)
        edit.modified = modified

        self.addTab(editor_widget, edit.filename)
        PluginInterface.show_log('info', 'CodeEditor', '新建文件')  # 2020-08-29 23:43:10 hzy INFO [CodeEditor]:新建文件
        self.setCurrentWidget(editor_widget)

    def open_file(self, path=''):
        path = QFileDialog.getOpenFileName(self, "选择打开的文件", '/home/hzy/Desktop', filter='*.py')[0]
        if os.path.exists(path):
            with open(path, 'r') as f:
                s = f.read()
            filename = os.path.basename(path)
            self.create_new_editor_tab(text=s, path=path, filename=filename, modified=False)

    def on_tab_close_request(self, close_index: int):
        tab_to_close = self.widget(close_index)
        tab_to_close.deleteLater()
        tab_to_close.edit.on_close_request()
        self.removeTab(close_index)

    def refresh_modified_status(self, filename: str):
        self.setTabText(self.currentIndex(), filename)
