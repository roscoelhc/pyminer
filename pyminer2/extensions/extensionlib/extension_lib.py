def wrapper():
    from pyminer2.extensions.extensions_manager.manager import extensions_manager
    class ExtensionLib:
        def get_interface(self,name):
            return extensions_manager.get_ext_by_name(name).public_interface
        def insert_widget(self,widget,insert_mode,config=None):
            extensions_manager.ui_inserters[insert_mode](widget,insert_mode,config)
    return ExtensionLib

extension_lib=wrapper()()